sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
	return AppComponent.extend("LAB.fiori_elements.Component", {
		metadata: {
			"manifest": "json"
		}
	});
});